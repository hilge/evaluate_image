# Evaluate images
This is a R shiny application to evaluate images in
the input folder as either true, possible or false (or marked for
further discussion), and save this evaluation along with the user who
has done them to a text file in the output folder. Each evaluation is
saved recoding the image name, user, and evalation, everytime a button
is pushed so there is no need to save the results in the end.

This application was developed to evaluate genomic plots of Copy
Number Variants (CNVs), but can be used for evaluation of any type of image.

## Screenshot 
![Screen shot](/data/example.png)

# Install
* download this repository to your local computer.
* Install R
* Install shiny within R, by running the command _install.packages("shiny")_

# Run app
## Within an R terminal
- Start an R session and set the working directory to the parent folder
 of this project.
- Run command: library("shiny")
- Run command: runApp("evaluate_image")

## In R-studio
- Open up the server.R file and push the button runApp.

