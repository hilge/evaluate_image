fluidPage(
  titlePanel("Evaluate image"),
  textOutput("n_images"),
  textOutput("image_name"),
  fluidRow(
      column(4,
             radioButtons("user", "Evaluator:",
                          c("Andy" = "andy", "Elvira" = "elvira", "Irina" = "irina", "Jasmine" = "jasmine", "Johan" = "johan", "Kate" = "kate", "Niamh" = "niamh", "Nonie" = "Nonie"), inline = T),
             actionButton("true", "True"),
             actionButton("possible", "Possible"),
             actionButton("false", "False"),
             actionButton("dodgy", "Dodgy"),
             actionButton("nextone", "Next"),
             actionButton("discuss", "Discuss"),
             imageOutput("image1", height = 300)
             )
  )
)


